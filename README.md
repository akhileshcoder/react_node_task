# KeyPoints

- `Standards and Design Pattern:` progressive web application (PWA) 
- `CSS Preprocessor:` PostCSS, PreCSS
- `View Library:` React
- `Backend:` nodeJS
- `Task Runner and Project Builder:` Webpack(client), Gulp(Server)
- `JavaScript Interface:` Typescript
- `Test Environment:` Mocha, Chai, Jest, testing-library/react
- caching of assets for full offline APP


# How to install and build:

- Step 1: download project from following link:

https://drive.google.com/drive/folders/12ekfz5KeTNiG4uBZFbs0jVK7rXkmC57K?usp=sharing

- Step 2: run following command in root directory and wait till finish

`sudo npm run start:all"`

- Step 3: open following link in browser:

http://localhost/

or

http://localhost:80/

Note: 
- preferred to use sudo as it needs permission to delete redundant directory
- You can change port and other config in `config` folder


# How to clean and rebuild

- Step 1: run following command in root directory and wait till finish

`sudo npm run restart:all"`



# How to test-cases

- Step 1: run following command in root directory and wait till finish

`npm run test:all"`

