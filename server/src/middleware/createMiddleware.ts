const bodyParser = require("body-parser"),
  cors = require("cors");
export const createMiddleware = (express, app, publicPath) => {
  app.use(cors());
  app.use(bodyParser.json({ limit: "5mb" }));
  app.use(bodyParser.urlencoded({ extended: true, limit: "5mb" }));
  app.use(express.static(publicPath));
  app.use('/',  (req, res, next) => {
    console.log('Requested resource: ', req.originalUrl, req.originalUrl.startsWith('/api/'));
    if(req.originalUrl.startsWith('/api/')) {
      next();
    } else {
      res.sendFile(`${publicPath}/index.html`);
    }
    next()
  });
};
