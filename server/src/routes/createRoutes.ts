import { getCar, getCars } from '../controllers/cars';
import { getColors } from '../controllers/colors';
import { getManufacturers } from '../controllers/manufacturers';

export const createRoutes = (app, publicPath) => {
  app.get("/api/cars/:carId", getCar);
  app.get("/api/cars*", getCars);
  app.get("/api/colors", getColors);
  app.get("/api/manufacturers", getManufacturers);
};
