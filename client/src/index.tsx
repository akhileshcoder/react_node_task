import React, {Fragment} from 'react';
import { render } from 'react-dom';
import { PageRoute } from './route';

render(<PageRoute />, document.getElementById('app'));


(function () {
  if ('serviceWorker' in window.navigator) {
    navigator.serviceWorker.register('./service-worker.js', {scope: '/'})
      .then(() => console.log('Service Worker registered successfully.'))
      .catch(error => console.log('Service Worker registration failed:', error));
  }
})();
