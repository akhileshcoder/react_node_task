import React, { Component, Fragment } from 'react';
import {
    BrowserRouter,
    Route,
    Switch,
    NavLink as Link
} from 'react-router-dom'

import {Dashboard, Purchase, Sell, NotFound, MyOrders} from '../views';
import { Header, Footer } from '../hoc';

export interface Props {

}

export class PageRoute extends Component<Props> {
    render () {
        return (
          <BrowserRouter>
              <Fragment>
                  <Header />
                  <Switch>
                      <Route exact path="/" component={Dashboard} />
                      <Route path="/favorites" component={MyOrders} />
                      <Route path="/purchase" component={Purchase} />
                      <Route path="/sell" component={Sell} />
                      <Route path="*" component={NotFound} />
                  </Switch>
                  <Footer/>
              </Fragment>
          </BrowserRouter>
        )
    }
}

export default PageRoute
