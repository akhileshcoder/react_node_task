import {Method, Request, RequestError, RequestResponse} from './request';
import {compare} from './object';

export {Method, Request, RequestError, RequestResponse, compare}

export default Request;
