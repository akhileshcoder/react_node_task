import axios, {AxiosError, AxiosResponse} from "axios";
import { setupCache } from 'axios-cache-adapter';

import {config} from '../config/configs';

const cache = setupCache({
  maxAge: 15 * 60 * 1000
});

const api = axios.create({
  adapter: cache.adapter
});

export type Method = 'get' | 'delete' | 'head' | 'options' | 'post' | 'put' | 'patch';

export const RequestResponse: AxiosResponse= (<AxiosResponse>{});
export const RequestError: AxiosError= (<AxiosError>{});
export class Request {
  method: Method;
  url: string;
  param: object;
  constructor(url:string, method:Method = 'get', param = {}) {
    const {origin, port} = config;
    if(!url.startsWith('http')){
      url = `${origin}:${port}${url.startsWith('/') ? '' : '/'}${url}`
    }
    this.method = method;
    this.url = url;
    this.param = param;
  }
  then(callback= (res?:AxiosResponse, err?: AxiosError) => {}){
    (async ()=>{
      try {
        const response = await api({
          method:this.method,
          url: this.url
        });
        callback(response, (<AxiosError>{}));
      } catch (error) {
        callback((<AxiosResponse>{}), error);
      }
    })();
  }
}
