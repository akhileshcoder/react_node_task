import React, { Component, Fragment } from 'react'
import { List } from '../../hoc/List';
import {localStorageKey} from "../../hoc/cars/ShowCar";

interface Props {

}
export class MyOrders extends Component<Props> {
  render() {
    const cars = JSON.parse(localStorage.getItem(localStorageKey) || "[]")
    const data = {
      totalCarsCount: cars.length,
      totalPageCount: 1,
      cars
    };
    return (<div className="page-container">
      <List listTitle="Saved cars" data={data}/>
    </div>);
  }
}

export default MyOrders
