import React, { Component, Fragment } from 'react';
import { Logo } from '../design/Logo';
import { Button } from '../design/Button';
import { NavLink as Link } from 'react-router-dom';
import { EmptyState } from '../design/empty-state';

interface Props {

}
export class NotFound extends Component<Props> {
  render () {
    return (
      <EmptyState
        emptyIcon={
          <Logo img="/images/logo.png" />
        } title="404 - Not Found"
        subtitle="Sorry, the page you are looking for does not exist."
        action={
          <Fragment>
            <span>You can always go back to the</span>
            <Button isText={true}>
              <Link to="/">
                homepage.
              </Link></Button>
          </Fragment>
        }
      />
    )
  }
}

export default NotFound
