import {manufacturer, filter} from "../../types"
const colorFormatter = (data:{colors: Array<string>}) =>(
  [
    {uuid: '', label: 'All car colors'},
    ...(data.colors || []).map(color => ({uuid: color, label: color}))]
);

const manufacturersFormatter = (data:manufacturer) => {
  const optionData: Array<{
    name: string, label: string, uuid: string, manufacturerName: string, modelNameUuid: string
  }> = [{
    name: '', manufacturerName: '', label: 'All manufacturers', modelNameUuid: '', uuid: ''
  }];
  (data.manufacturers || []).forEach((manufacturer) => {
    manufacturer.models.forEach((model:{name:string, uuid:string})=> {
      optionData.push({
        name: model.name,
        label: `${manufacturer.name} | ${model.name}`,
        uuid: model.uuid,
        manufacturerName: manufacturer.name,
        modelNameUuid: manufacturer.uuid
      })
    })
  });
  return optionData;
};

export const defaultFilters: Array<filter> = [
  {
    value: "",
    formatter: colorFormatter,
    url: "/api/colors",
    name: "color",
    label: "Color",
    loadingText: "Loading colors"
  },
  {
    value: "",
    formatter: manufacturersFormatter,
    url: "/api/manufacturers",
    name: "manufacturers",
    label: "Manufacturers",
    loadingText: "Loading manufacturers"
  }
];
