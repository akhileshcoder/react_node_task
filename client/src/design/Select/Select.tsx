import React, { FormEvent, PureComponent } from 'react';

export type optionType = { name?: string, label: string, uuid: string, value?: string, hidden?: boolean, disabled?: boolean, type?: string };
export const defaultOption = { label: '', uuid: '' };
export interface Props {
  label?: string
  name: string
  multi?: boolean
  data?: Array<string | optionType>
  options?: Array<string | optionType>
  selectedOption?: string | optionType
  onChange: Function
  formatter?: Function
}

export class Select extends PureComponent<Props> {
  _isMounted = false;
  componentWillUnmount() {
    this._isMounted = false;
  }
  componentDidMount(): void {
    this._isMounted = true;
  }
  state = {
    selectedOption: this.props.selectedOption || '',
    options: [defaultOption]
  };

  constructor(props: Props) {
    super(props);
    let {selectedOption, options, data, formatter} = this.props;
    options = options || data || [];
    this.state = {
      selectedOption: selectedOption || '',
      options: formatter ? formatter(options) : options
    };
  }

  render() {
    const { label } = this.props;
    const { selectedOption, options } = this.state;
    return (
      <div className="form-group">
        <label>
          {label ? <div className="form-label">{label}</div> : null}
          <select className="form-select" value={
            typeof selectedOption === 'string'
              ? selectedOption : selectedOption.uuid
          } onChange={this.handleChange}>
            {options.map((option, ind) => <option key={ind}
                                                  value={option.uuid}>
              {option.label}
            </option>)}
          </select>
        </label>
      </div>
    );
  }

  handleChange = (event: FormEvent) => {
    const { options } = this.state;
    // @ts-ignore
    const { value } = event.target;
    this._isMounted && this.setState({ selectedOption: value });
    this.props.onChange(this.props.name, options.find(opt => opt.uuid === value));
  };
}
