import { cleanup, render } from '@testing-library/react';
import { Button, Props } from './Button';
import React, {PureComponent} from 'react';

describe('<Button />', () => {
  afterEach(cleanup);
  function renderButton(props: Partial<Props> = {}) {
    const renderedElm = render(<Button {...props}>Test</Button>);
    const { asFragment } = renderedElm;
    expect(asFragment()).toMatchSnapshot();
    return renderedElm;
  }
  it("render default", () => {
    renderButton({});
  });
});
