import React, {ReactElement, PureComponent} from 'react';
import cns from 'classnames';

export interface Props {
    title?: string | ReactElement,
    body?: string | ReactElement,
    footer?: string | ReactElement,
    isBorderLess?: boolean,
    className?: string,
    titleClassName?: string,
    bodyClassName?: string
}
export class Card extends PureComponent<Props> {
    render() {
        const {
            title,
            body,
            footer,
            isBorderLess,
            className,
            titleClassName,
            bodyClassName,
        } = this.props;
        return (
            <div className={cns('card-wrapper', className, { 'card-border': !isBorderLess })}>
                <div>
                    {title &&
                        (typeof title === 'string' ? (
                            <div className={cns('card-li', 'card-title', titleClassName)}>
                                {title}
                            </div>
                        ) : (
                            <div className={cns('card-li', 'card-title')}> {title} </div>
                        ))}
                    {body &&
                        (typeof body === 'string' ? (
                            <div className={cns('card-li', 'card-body', bodyClassName)}>{body}</div>
                        ) : (
                            <div className={cns('card-li', 'card-body')}> {body} </div>
                        ))}
                    {footer &&
                        (typeof footer === 'string' ? (
                            <div className={cns('card-li', 'card-foot')}> {footer} </div>
                        ) : (
                            <div className={cns('card-li', 'card-foot')}> {footer} </div>
                        ))}
                </div>
                <div />
            </div>
        );
    }
}
