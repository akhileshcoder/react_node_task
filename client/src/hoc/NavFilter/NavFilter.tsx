import React, { Fragment, PureComponent } from 'react';
import { RequestLoader } from '../../design/RequestLoader';
import { Button, Select } from '../../design';

import {Ifilters} from "../../types";

export interface Props {
  onFilterChange: Function,
  filters: Ifilters
}

export class NavFilter extends PureComponent<Props> {
  _isMounted = false;
  state = {
    filters: this.props.filters
  };
  render() {
    return <div className="nav">
      <div className="nav-group">
        {
          this.state.filters.map(({value, formatter, url, name, label, loadingText}, ind)=> (
            <div className="nav-item" key={ind}>
              <RequestLoader url={url} loadingText={loadingText}>
                <Select name={name} onChange={(name:string, value:string) => this.onFilterChange(ind, name, value)} formatter={formatter} label={label} />
              </RequestLoader>
            </div>
          ))
        }
        <div className="nav-item">
          <Button className="pull-right" onClick={this.onFilterSubmit}> Filter </Button>
        </div>
      </div>
    </div>;
  }
  componentWillUnmount() {
    this._isMounted = false;
  }
  componentDidMount(): void {
    this._isMounted = true;
  }
  onFilterChange =  (ind: number, name:string, value:string)=> {
    const {filters} = this.state;
    filters[ind].value = value;
    this._isMounted && this.setState(filters);
  };
  onFilterSubmit =  ()=> {
    this.props.onFilterChange(this.state.filters);
  };
}
