// ETA: 2 hours
import { cleanup, findAllByText, fireEvent, render, waitForElement } from '@testing-library/react';
import { NavFilter, Props } from './NavFilter';
import React from 'react';

import {defaultFilters} from "../../views/Dashboard/filterUitls";
describe('<NavFilter />', () => {
  afterEach(cleanup);
  function renderNavFilter(props: Partial<Props> = {}) {
    const defaultProps = {  filters: defaultFilters, onFilterChange: () => {}};
    const renderedElm = render(<NavFilter {...defaultProps} {...props} />);

    const { asFragment } = renderedElm;
    expect(asFragment()).toMatchSnapshot();
    return renderedElm;
  }
  it("renders default", () => {
    renderNavFilter();
  });
  const findItemByText =  (text: string) => {
    return async () => {
      const { findByText } = renderNavFilter();
      const navItem = await findByText(text);
      fireEvent.click(navItem);
      const container = document.body;
      const textNodes = await waitForElement(() => findAllByText(container, text));
      (await textNodes).forEach(textNode => {
        expect(textNode).toBeVisible();
      })
    }
  };
  it('should render "Color"', findItemByText('Loading colors ...'));
  it('should render "Manufacturer"', findItemByText('All car colors'));
  it('should render "Filter"', findItemByText('Filter'));
});
