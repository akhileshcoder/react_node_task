import React from 'react';
import { cleanup, findAllByText, fireEvent, render, waitForElement } from '@testing-library/react';

import {ShowCar, Props} from "./ShowCar";

import { defaultICar, ICar } from '../../../types';

describe('<ShowCar />', () => {
  afterEach(cleanup);
  const defaultPropVal = {
    details: defaultICar,
    onClose: () => {}
  };
  function renderShowCar(props: Partial<Props> = {}) {
    const renderedElm = render(<ShowCar {...defaultPropVal} {...props} />);

    const { asFragment } = renderedElm;
    expect(asFragment()).toMatchSnapshot();
    return renderedElm;
  }
  it("renders default", () => {
    renderShowCar();
  });
  const findItemByText =  (text: string) => {
    return async () => {
      const { findByText } = renderShowCar();
      const navItem = await findByText(text);
      fireEvent.click(navItem);
      const container = document.body;
      const textNodes = await waitForElement(() => findAllByText(container, text));
      (await textNodes).forEach(textNode => {
        expect(textNode).toBeVisible();
      })
    }
  };

  it('should render car manufacturer name', findItemByText('A4'));

  it('should render car model name, stock number, mileage, fule type, color', findItemByText('Stock #1 - 1km - Petrol - red'));

  it('should render "Close" to close model <Button />', findItemByText('Close'));
});
