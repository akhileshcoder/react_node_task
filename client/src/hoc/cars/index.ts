import {CarDetails, Car} from './Car';
import {ShowCar} from './ShowCar';

export {CarDetails, ShowCar, Car}

export default Car;
