import { cleanup, render } from '@testing-library/react';
import { Car } from './Car';
import { defaultICar, ICar } from '../../../types';
import React from 'react';

describe('<Car />', () => {
  afterEach(cleanup);
  function renderCar(props: ICar) {
    const renderedElm = render(<Car details={props} />);

    const { asFragment } = renderedElm;
    expect(asFragment()).toMatchSnapshot();
    return renderedElm;
  }
  it("renders default", () => {
    renderCar(defaultICar);
  });
});
