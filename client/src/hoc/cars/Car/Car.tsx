import React, { Fragment, PureComponent } from 'react';
import { Card, Img, Button } from '../../../design';
import { ICar } from '../../../types';
import { ShowCar } from '../ShowCar';

export interface CarDetails {
  totalCarsCount: number,
  totalPageCount: number
  cars?: Array<ICar>
}

export interface Props {
  details: ICar
}
export class Car extends PureComponent<Props> {
  _isMounted = false;
  state = {
    isPreview: false
  };
  render() {
    const {details} = this.props;
    return (
      this.state.isPreview ? (<ShowCar details={details} onClose={this.togglePreview}/>) : (
        <div className="car-tile">
          <Img src={details.pictureUrl} alt={details.modelName}/>
          <Card
            title={details.modelName}
            body={`Stock #${details.stockNumber} - ${details.mileage.number}${details.mileage.unit} - ${details.fuelType} - ${details.color}`}
            footer={<Button onClick={this.togglePreview} isText={true}> View details </Button>}
            isBorderLess={true}/>
        </div>
      )
    );
  }
  componentWillUnmount() {
    this._isMounted = false;
  }
  componentDidMount(): void {
    this._isMounted = true;
  }
  togglePreview = () => {
    this._isMounted && this.setState({isPreview: !this.state.isPreview});
  }
}

