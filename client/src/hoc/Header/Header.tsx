import React, { PureComponent } from 'react';
import { NavLink as Link } from 'react-router-dom';
import { Logo, Tab } from '../../design';

export interface Props {
}

const pages = [
  { link: `/i-am-a-random-url`, label: 'Random' },
  { link: '/', label: 'Dashboard' },
  { link: '/purchase', label: 'Purchase' },
  { link: '/favorites', label: 'My Orders' },
  { link: '/sell', label: 'Sell' }
];

export class Header extends PureComponent<Props> {
  render() {
    return <div className="header">
      <Logo img="/images/logo.png"/>
      <Tab>
        {
          pages.reverse().map((page, ind) => <Link key={ind} to={page.link}>
            {page.label}
          </Link>)
        }
      </Tab>
    </div>;
  }
}
